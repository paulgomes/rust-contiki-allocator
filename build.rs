extern crate gcc;

fn main() {
    // TODO: make this link with my libmemb.a
    // Later I can figure out how to make it link with memb when using my allocator with Contiki

    gcc::compile_library("libmembWrapper.a", &["src/membWrapper.c"]);
}