#include <stdio.h>

void hello() {
    printf("Hello, World! ... from C\n");
}

// TODO: Modify this file to be a wrapper around Contiki memb
// The thought is I can create a simple wrapper that is easy for Rust to invoke
// that hides the complexity of the C macros that are used by Contiki memb

// First just steal some code from contiki-cstring and make Rust unit test invoke that code
// that then invokes methods in my locally built libmemb.a