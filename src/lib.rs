// TODO: Morph this into a Rust allocator


#[cfg(test)]
mod tests {
    extern { fn hello(); }

    #[test]
    fn it_works() {
        unsafe { hello(); }
    }
}
